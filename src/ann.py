import argparse
import sys
import numpy as np
import matplotlib.pyplot as plt

__author__ = "Erik Nadel, Sai Kiran Vadlamudi"
__email__ = "einadel@wpi.edu, svadlamudi@wpi.edu"


class datapoint:
    def __init__(self,x, y, label):
        self.x = float(x)
        self.y = float(y)
        self.label = float(label)

    def __str__(self):
        return "(x: %f, y: %f) - label: %d\n" % (self.x, self.y, self.label)


class Neural_Network(object):
    def __init__(self, hiddenNodes):
        self.inputLayerSize = 2;
        self.hiddenLayerSize = hiddenNodes;
        self.outputLayerSize = 1;

        self.weights_to_hidden = np.random.randn(self.inputLayerSize, self.hiddenLayerSize)
        self.weights_to_output = np.random.randn(self.hiddenLayerSize, self.outputLayerSize)

    # Calculate the output based on given inputs
    def forward(self, X):
        # Calculate input multiplied by the weights of the synapses
        self.input_at_hidden = np.dot(X, self.weights_to_hidden)

        # Calculate activation on the input multiplied by weights
        self.output_at_hidden = self.sigmoid(self.input_at_hidden)

        # Calculate hidden layer output multiplied by synapses
        self.input_at_output = np.dot(self.output_at_hidden, self.weights_to_output)

        # Calculate activation of the output
        output = self.sigmoid(self.input_at_output)
        return output

    # Activation function
    def sigmoid(self, z):
        return 1/(1+np.exp(-z))

    # Derivative of the activation, sigmoid, function
    def derivative_of_sigmoid(self, z):
        return np.exp(-z)/((1 + np.exp(-z)) ** 2)

    # Calculate error between actual, calculated output, and expected, given output.
    def cost_function(self, training_data_output):
        return 0.5 * sum((training_data_output - self.output) ** 2)

    # Train the network with the given training data with input and expected output
    def train_network(self, training_data_input, training_data_output):
        prev_cost = sys.maxint

        for _ in range(0, 10000, 1):
            # Propagate the inputs forward
            self.output = self.forward(training_data_input)

            # Check cost to see if it is smaller than previous cost
            cost = self.cost_function(training_data_output)
            if prev_cost < cost:
                break
            prev_cost = cost

            # Propagate error backward from output to hidden
            delta3 = np.multiply(-(training_data_output - self.output), self.derivative_of_sigmoid(self.input_at_output))
            dJdW2 = np.dot(self.output_at_hidden.T, delta3)

            # Propagate error backward from hidden to input
            delta2 = np.dot(delta3, self.weights_to_output.T) * self.derivative_of_sigmoid(self.input_at_hidden)
            dJdW1 = np.dot(training_data_input.T, delta2)

            # Update weights
            self.weights_to_hidden -= 0.02 * dJdW1
            self.weights_to_output -= 0.02 * dJdW2

    # Classify given test input and calculate the error rate based on given test output
    def classify_set(self, test_input, test_output):
        # Calculate output
        self.output = self.forward(test_input)
        num_wrong = 0

        # Check number of wrong classifications based on given expected output
        test_results = np.zeros(shape=(len(test_out), 1), dtype=np.int)
        for i in range(0, len(test_output), 1):
            if np.round(self.output[i] - 0.1) == test_output[i]:
                test_results[i] = 1
            else:
                num_wrong += 1

        # Return the actual classifications, incorrect classifications, error rate
        return self.output, test_results, (num_wrong + 0.0) / len(test_output) * 100

# Parse given file to an array of data points
def map_data_to_label(input_file):
    with open(input_file, 'r') as o:
        file_data = o.read()

    data_points = []

    file_data = file_data.split('\n')
    for data_point in file_data:
        data_point = data_point.replace('\r', '')
        if len(data_point.split()) > 1:
            x = data_point.split(' ')[0]
            y = data_point.split(' ')[1]
            label = data_point.split(' ')[2]
            data_points.append(datapoint(x, y, label))

    return data_points


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Artifical Neural Network", add_help=False)
    parser.add_argument('file', metavar='FILE', help="file to read")
    parser.add_argument('-h', type=int, help='Number of hidden nodes to use', default=5)
    parser.add_argument('-p', type=float, help='Percent of data to withhold for testing', default=0.2)

    args = parser.parse_args()

    print args

    args = vars(args)
    percent_test = args.get('p')

    # Read all data points from file and parse
    data_points = map_data_to_label(args['file'])
    num_test_data = len(data_points) * percent_test
    num_train_data = len(data_points) - num_test_data

    train_in = np.zeros(shape=(num_train_data, 2))
    train_out = np.zeros(shape=(num_train_data, 1), dtype=np.int)
    test_in = np.zeros(shape=(num_test_data, 2))
    test_out = np.zeros(shape=(num_test_data, 1), dtype=np.int)

    # Separate test and training data from given set of data
    i = 0
    j = 0
    for data_point in data_points:
        if i < num_train_data:
            train_in[i] = [data_point.x, data_point.y]
            train_out[i] = data_point.label
            i += 1
        else:
            test_in[j] = [data_point.x, data_point.y]
            test_out[j] = data_point.label
            j += 1

    # Train Network
    NN = Neural_Network(args.get('h'))
    NN.train_network(train_in, train_out)

    # Test Network
    actual_out, incorrect_points, error_rate = NN.classify_set(test_in, test_out)
    print NN.cost_function(test_out)

    # Print Results
    print "Error Percentage: %.2f%%" % error_rate

    # Show Graphs of Classifications
    color = {1: "green", 0: "red"}

    plt.figure(1)
    plt.subplot(211)
    plt.scatter(test_in[:, 0], test_in[:, 1], c=[color[train_in[0]] for train_in in incorrect_points], s=25)
    plt.title('Classification Results (Green:Correct, Red:Wrong)')

    plt.subplot(212)
    plt.scatter(test_in[:, 0], test_in[:, 1], c=[color[np.round(train_in[0] - 0.1)] for train_in in actual_out], s=25)
    plt.title('Classification Divide (1:Green, 0:Red)')
    plt.show()





